import { Component } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template: `<p>This is a warning message.</p>`,
  styles: [
    `
        p {
          background-color: palevioletred;
          border: 1px solid red;
          padding: 20px;
          color: white;
        }
    `
  ]
})

export class WarningAlertComponent {

}
